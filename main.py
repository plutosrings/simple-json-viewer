import logging
import os
import pprint
import json

from json.decoder import JSONDecodeError
import argparse

from PyQt5 import QtGui, QtWebKit, QtCore


from PyQt5.QtGui import QKeySequence, QIcon

from PyQt5.QtWidgets import QPushButton, QLabel, QWidget, QGroupBox, QMainWindow, QApplication, QAction, qApp, \
                            QFileDialog, QGridLayout, QHBoxLayout, QLineEdit, QListWidget, QListWidgetItem, \
                            QMessageBox, QScrollArea, QSizePolicy, QTabWidget, QVBoxLayout, QShortcut
from PyQt5.QtWebKitWidgets import QWebView, QWebPage


import sys

__author__ = 'joe'




class GrxeWebPage(QWebPage):

    def __init__(self, logger=None, parent=None):
        super(GrxeWebPage, self).__init__(parent)
        if not logger:
            logger = logging

        self.logger = logger

        #self.prep_html()

    def javaScriptConsoleMessage(self, msg, lineNumber, sourceID):
        self.logger.warning("JsConsole(%s:%d): %s" % (sourceID, lineNumber, msg))



class MainWindow(QMainWindow):

    window_title = "Simple_JSON_Viewer by GRXE"

    def __init__(self, args, parent=None):
        super(MainWindow,self).__init__(parent)



        self.setWindowTitle(self.window_title)
        self.createActions()
        self.createMenus()

        layout  = QVBoxLayout()
        widget  = QWidget()

        self.webview    = QWebView()
        self.debug_page = GrxeWebPage()
        self.webview.setPage(self.debug_page)

        refresh_seq = QShortcut(QKeySequence(QtCore.Qt.Key_F5), self)

        #QtCore.connect(refresh_seq, QtCore.pyqtSignal('activated()'), self.refresh_webview)
        refresh_seq.activated.connect(self.refresh_webview)

        layout.addWidget(self.webview)

        widget.setLayout(layout)

        self.setCentralWidget(widget)

        if args.json_file:
            self.open_json_file(args.json_file)

    def load_json(self, json_file):
        with open(json_file, "r") as json_file_handle:
            json_data = json.load(json_file_handle)
            json_file_handle.close()
            return json_data

    #def render_json_to_html(self, json_data):

    #    pass


    def render_json_to_html(self, json_data, html_list):

            if isinstance(json_data, dict):
                html_list.append("""<ul>""")

                for key, value in json_data.items():
                    html_list.append("""<li>%s:  """ % key)

                    if isinstance(value, dict) or isinstance(value, list):
                        self.render_json_to_html(value, html_list)

                    else:
                        html_list.append("""%s </li>""" % value)

                    html_list.append("""</li>""")

                html_list.append("""</ul>""")

            elif isinstance(json_data, list):
                html_list.append("""<ul> """)

                for item in json_data:
                    html_list.append("""{  """)

                    self.render_json_to_html(item, html_list)

                    html_list.append("""},</br> """)

                html_list.append("""</ul>""")

            else:
                html_list.append("""<li>%s</li>""" % json_data)

    def refresh_webview(self):
        print("refreshing!")

        self.open_json_file(self.json_file_name)

        # self.webview.setUrl(QtCore.QUrl("file://%s" % html_file))
        #self.webview.setPage(self.debug_page)
        #self.webview.setHtml(html_lines, baseUrl=QtCore.QUrl("file:///%s/" % self.json_file_path))

        #self.statusBar().showMessage("Refreshed %s" % self.json_file_name)

    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.open_html_act)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.quit_act)

        self.menuBar().addSeparator()

        self.helpMenu = self.menuBar().addMenu("&Help")
        self.helpMenu.addAction(self.about_act)
        self.helpMenu.addAction(self.aboutQt_act)

    def createActions(self):
        self.open_html_act = QAction(QIcon(':/images/open.png'),
                "&Open HTML", self, shortcut=QKeySequence.Open,
                                     statusTip="Open about HTML File",
                                     triggered=self.open_json_file)

        self.quit_act = QAction("&Quit", self, shortcut="Ctrl+Q",
                statusTip="Quit the application", triggered=self.close)

        self.about_act = QAction("&About", self,
                statusTip="Show the application's About box",
                triggered=self.about)

        self.aboutQt_act = QAction("About &Qt", self,
                statusTip="Show the Qt library's About box",
                triggered= qApp.aboutQt)

    def open_json_file(self, filename=None):

        if not filename:
            file_dialog     = QFileDialog()
            self.json_file_name = file_dialog.getOpenFileName(self, "Open JSON file",
                                                     QtCore.QDir.currentPath(), filter="JSON Files (*)")[0]
        else:
            self.json_file_name = filename

        if self.json_file_name:
            #file_path = "%s/%s" % ( html_file_full_path, html_file )
            #html_file_path =
            self.json_file_path = os.path.dirname(os.path.realpath(self.json_file_name))
            pprint.pprint("File name from dialog: %s" % self.json_file_name)

            print("File Path: %s" % (self.json_file_path))

            html_lines = []
            html_lines.append("""<html>""")
            html_lines.append("""<div id="json-data">""")
            try:
                self.render_json_to_html(self.load_json(self.json_file_name), html_lines)
            except JSONDecodeError as jde:
                QMessageBox.about(self, "Invalid JSON File!", """
                Please supply a JSON formatted document file.
                """)

            html_lines.append("""</div>""")
            html_lines.append("""</html>""")
            #self.webview.setUrl(QtCore.QUrl("file://%s" % html_file))
            self.webview.setPage(self.debug_page)
            self.webview.setHtml("".join(html_lines), baseUrl=QtCore.QUrl("file:///%s/" % self.json_file_path))

            self.statusBar().showMessage("Loaded %s" % self.json_file_path)

    def about(self):
        QMessageBox.about(self,"About Simple_JSON_Viewer","""
<b>Simple_JSON_Viewer</b> is a simple testing platform for HTML/Javascript in QT Webkit%s
Developer/Author: Joe Payne (GRXE)
""" % os.linesep)

    def read_file(self, filename):
        tar_file = open(filename)

        lines = tar_file.readlines()
        tar_file.close()

        return ''.join(lines)


class Simple_JSON_Viewer():

    rendering_html = """

    """



    def __init__(self):
        super(Simple_JSON_Viewer, self).__init__()

        app = QApplication(sys.argv)

        parser = argparse.ArgumentParser( description="Payce Command Line App")
        #parser.add_argument("action", choices=self.operations)

        parser.add_argument("-f","--json-file")

        args = parser.parse_args()

        mainwindow = MainWindow(args)
        mainwindow.show()



        #mainwindow.webView.load(QUrl("charting.html"))
        #mainwindow.webView.


        mainwindow.show()




        sys.exit(app.exec_())



if __name__ == '__main__':

    player = Simple_JSON_Viewer()


